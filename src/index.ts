//----------------------------
//      every challenge
//----------------------------
// implement the `every` function
// add types and generics as needed
import { predicate } from "./types.js";



const students = ["jOHn", "DAn", "ruTH", "jAnE", "beN"];

const hasN = (str:string):boolean => str.toLowerCase().includes("n");
const min3Chars = (str:string):boolean => str.length >= 3;

const ages = [72, 16, 22, 42, 36, 11, 52];
const over21 = (n:number):boolean => n > 21;

//implement the `every` function
const every = <T> (items:T[], cb:predicate):boolean => {
    let flag = true;
    items.forEach((item:T)=>{  
        if(!cb(item)){
            flag = false
        }
    })
    return flag
};

const all_students_have_n = every(students, hasN);
const all_students_min_3 = every(students, min3Chars);
const all_ages_over_21 = every(ages, over21);

console.log({ all_students_have_n }); // -> false
console.log({ all_students_min_3 }); // -> true
console.log({ all_ages_over_21 }); // -> false
